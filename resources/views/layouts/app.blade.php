<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');
            .font-family-karla { font-family: karla; }
            .bg-sidebar { background: #3d68ff; }
            .cta-btn { color: #3d68ff; }
            .upgrade-btn { background: #1947ee; }
            .upgrade-btn:hover { background: #0038fd; }
            .active-nav-link { background: #1947ee; }
            .nav-item:hover { background: #1947ee; }
            .account-link:hover { background: #3d68ff; }
        </style>
    </head>
    <body class="bg-gray-100 font-family-karla flex">
        <aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
            <nav class="text-white text-base font-semibold pt-3">
                <a href="{{ route('products.index') }}" class="flex items-center {{ request()->routeIs('products.*') ? 'active-nav-link' : 'opacity-75 hover:opacity-100'}} text-white py-4 pl-6 nav-item">
                    <i class="fas fa-sticky-note mr-3"></i>
                    Products
                </a>
                <a href="{{ route('boxes.index') }}" class="flex items-center text-white {{ request()->routeIs('boxes.*') ? 'active-nav-link' : 'opacity-75 hover:opacity-100'}} py-4 pl-6 nav-item">
                    <i class="fas fa-table mr-3"></i>
                    Boxes
                </a>
                <a href="{{ route('suppliers.index') }}" class="flex items-center text-white {{ request()->routeIs('suppliers.*') ? 'active-nav-link' : 'opacity-75 hover:opacity-100'}} py-4 pl-6 nav-item">
                    <i class="fas fa-align-left mr-3"></i>
                    Suppliers
                </a>
            </nav>
        </aside>
        <div class="relative w-full flex flex-col h-screen overflow-y-hidden">
            <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
                @if ($message = \Session::get('success'))
                    <div class="bg-green-500 border-t border-b border-green-500 text-green-100 px-4 py-3" role="alert">
                        <p class="font-bold">{{ $message }}</p>
                    </div>
                @endif


                @if ($message = \Session::get('error'))
                    <div class="bg-red-500 border-t border-b border-red-500 text-red-100 px-4 py-3" role="alert">
                        <p class="font-bold">{{ $message }}</p>
                    </div>
                @endif


                @if ($message = \Session::get('warning'))
                        <div class="bg-yellow-500 border-t border-b border-yellow-500 text-yellow-100 px-4 py-3" role="alert">
                            <p class="font-bold">{{ $message }}</p>
                        </div>
                @endif


                @if ($message = \Session::get('info'))
                        <div class="bg-blue-500 border-t border-b border-blue-500 text-blue-100 px-4 py-3" role="alert">
                            <p class="font-bold">{{ $message }}</p>
                        </div>
                @endif
                <main class="w-full flex-grow p-6">
                    <h1 class="text-3xl text-black pb-6">{{ ucfirst(explodeImplodeString('.', ' - ', \Route::current()->getName())) }}</h1>
                    @yield('content')
                </main>
            </div>
        </div>
    </body>
</html>
