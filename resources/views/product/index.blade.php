@extends('layouts.app')

@section('content')
    <a class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800"
       href="{{ route('products.create') }}">Create new Product</a>
    <form class="flex items-end" method="get" action="{{ route('products.index') }}">
        <select id="order_by"
                class="mb-1 mr-2 inline-flex h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                name="order_by">
            <option value="">Order by ID ASC</option>
            <option {{ request('order_by') == 'id,desc' ? 'selected' : '' }} value="id,desc">Order by ID DESC</option>
            <option {{ request('order_by') == 'updated_at' ? 'selected' : '' }} value="updated_at">Order by Updated at ASC</option>
            <option {{ request('order_by') == 'updated_at,desc' ? 'selected' : '' }} value="updated_at,desc">Order by Updated at DESC</option>
        </select>
        <input name="search"
               class="mb-1 inline-flex h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
               type="text" placeholder="Search by ID, Name or Supplier" id="name" value="{{ request('search') }}"/>
        <div class="ml-3">
            <label class="mb-1 mr-2" for="in_storage">In storage</label>
            <input name="in_storage"
                   class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                   type="checkbox" id="in_storage" {{ request('in_storage') ? 'checked' : '' }} value="1"/>
        </div>
        <button
            class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800"
            type="submit">Use filters
        </button>
    </form>
    <div class="bg-white overflow-auto">
        <table class="min-w-full bg-white">
            <thead class="bg-gray-800 text-white">
            <tr>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">ID</th>
                <th class="text-left max-w-300 py-3 px-4 uppercase font-semibold text-sm">Photo</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">In storage</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Price</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Supplier
                </td>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Updated at
                </td>
            </tr>
            </thead>
            <tbody class="text-gray-700">
            @foreach($products as $key => $product)
                <tr @if($key%2 == 0) class="bg-gray-200" @endif>
                    <td class="text-left py-3 px-4">{{ $product->id }}</td>
                    <td class="text-left max-w-300 py-3 px-4">{!! !empty($product->photo) ? '<img class="product_image" src="'.Storage::url($product->photo).'" />' : '' !!}</td>
                    <td class="text-left py-3 px-4"><a
                            href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->name }}</a>
                    </td>
                    <td class="text-left py-3 px-4">{{ $product->in_storage ? 'yes' : 'no' }}</td>
                    <td class="text-left py-3 px-4">{{ $product->price }}</td>
                    <td class="text-left py-3 px-4">{{ $product->supplier->name }}</td>
                    <td class="text-left py-3 px-4">{{ $product->updated_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $products->appends(request()->toArray())->links() }}
@endsection
