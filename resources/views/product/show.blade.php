@extends('layouts.app')

@section('content')
    <div class="flex items-center justify-between">
        <a class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800"
           href="{{ route('products.edit', ['product' => $product->id]) }}">Edit Product</a>
        <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
            @csrf
            @method('DELETE')
            <button class="inline-flex items-center h-8 px-4 m-2 text-sm text-red-100 duration-150 bg-red-700 rounded-lg focus:shadow-outline hover:bg-red-800" type="submit">Delete</button>
        </form>
    </div>
    <div class="flex items-start justify-between">
        <div style="width: 48%">
            {!! !empty($product->photo) ? '<div class="mt-6 mb-6 text-2xl"><div><strong>Photo:</strong></div><img src="'.Storage::url($product->photo).'" /></div>' : '' !!}
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">Name:</strong> {{ $product->name }}</div>
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">In storage:</strong> {{ $product->in_storage ? 'yes' : 'no' }}</div>
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">Price:</strong> {{ $product->price }}</div>
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">Supplier:</strong> {{ $product->supplier->name }}</div>
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">Updated at:</strong> {{ $product->updated_at }}</div>
        </div>
        <div style="width: 48%">
            <div class="flex items-center mt-6 mb-6 text-2xl"><strong class="mr-2">In Boxes:</strong></div>
            <table class="min-w-full bg-white">
                <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">ID</th>
                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Discount</th>
                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Number of Products</th>
                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Created at
                    </td>
                </tr>
                </thead>
                <tbody class="text-gray-700">
                @foreach($boxes as $key => $box)
                    <tr @if($key%2 == 0) class="bg-gray-200" @endif>
                        <td class="text-left py-3 px-4">{{ $box->id }}</td>
                        <td class="text-left py-3 px-4">{{ $box->name }}</td>
                        <td class="text-left py-3 px-4">{{ $box->discount }}</td>
                        <td class="text-left py-3 px-4">{{ $box->products()->count() }}</td>
                        <td class="text-left py-3 px-4">{{ $box->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $boxes->links() }}
        </div>
    </div>
@endsection
