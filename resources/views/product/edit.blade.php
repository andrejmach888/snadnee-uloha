@extends('layouts.app')

@section('content')
    @if($errors->any())
        <ul style="color:#ff0000">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <form class="space-y-4 text-gray-700" method="post" action="{{ route('products.update', ['product' => $product->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="name">Name</label>
                <input name="name" required
                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                       type="text" id="name" value="{{ $product->name }}"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            {!! !empty($product->photo) ? '<div class="w-full"><img class="product_image" src="'.Storage::url($product->photo).'" /></div>' : '' !!}
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="photo">Photo</label>
                <input name="photo"
                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                       type="file" id="photo" accept="image/x-png,image/gif,image/jpeg"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="in_storage">In storage</label>
                <input name="in_storage"
                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                       type="checkbox" id="in_storage" {{ $product->in_storage ? 'checked' : '' }} value="1"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="price">Price</label>
                <input name="price"
                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                       type="number" step="any" required id="price" value="{{ $product->price }}"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-80">
                <label class="block mb-1" for="supplier_id">Supplier</label>
                <select id="supplier_id" class="select2" required name="supplier_id">
                    <option value="{{ $product->supplier_id }}">{{ $product->supplier->name }}</option>
                </select>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <button type="submit"
                        class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800">
                    Update
                </button>
            </div>
        </div>
    </form>
    <script>
        $(document).ready(function () {
            $('.select2').select2({
                width: '100%',
                ajax: {
                    method: 'get',
                    url: '{{ route('suppliers.jsonData') }}',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data, params) {
                        console.log(data.data);
                        return {
                            results: data.data
                        };
                    }
                }
            });
        });
    </script>
@endsection
