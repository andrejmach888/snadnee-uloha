@extends('layouts.app')

@section('content')
    <div class="bg-white overflow-auto">
        <table class="min-w-full bg-white">
            <thead class="bg-gray-800 text-white">
            <tr>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">ID</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Created at</td>
            </tr>
            </thead>
            <tbody class="text-gray-700">
            @foreach($suppliers as $key => $supplier)
            <tr @if($key%2 == 0) class="bg-gray-200" @endif>
                <td class="text-left py-3 px-4">{{ $supplier->id }}</td>
                <td class="text-left py-3 px-4">{{ $supplier->name }}</td>
                <td class="text-left py-3 px-4">{{ $supplier->created_at }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $suppliers->links() }}
@endsection
