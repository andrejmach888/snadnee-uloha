@extends('layouts.app')

@section('content')
    <a class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800"
       href="{{ route('boxes.create') }}">Create new Box</a>
    <div class="bg-white overflow-auto">
        <table class="min-w-full bg-white">
            <thead class="bg-gray-800 text-white">
            <tr>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">ID</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Discount</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Number of Products</th>
                <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Created at
                </td>
            </tr>
            </thead>
            <tbody class="text-gray-700">
            @foreach($boxes as $key => $box)
                <tr @if($key%2 == 0) class="bg-gray-200" @endif>
                    <td class="text-left py-3 px-4">{{ $box->id }}</td>
                    <td class="text-left py-3 px-4">{{ $box->name }}</td>
                    <td class="text-left py-3 px-4">{{ $box->discount }}</td>
                    <td class="text-left py-3 px-4">{{ $box->products()->count() }}</td>
                    <td class="text-left py-3 px-4">{{ $box->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $boxes->links() }}
@endsection
