@extends('layouts.app')

@section('content')
    @if($errors->any())
        <ul style="color:#ff0000">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <form class="space-y-4 text-gray-700" method="post" action="{{ route('boxes.store') }}">
        @csrf
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="name">Name</label>
                <input name="name" value="{{ old('name') }}" required class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline" type="text" id="name"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <label class="block mb-1" for="discount">Discount</label>
                <input name="discount" value="{{ old('discount') }}" required class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline" type="number" step="any" id="discount"/>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-80">
                <label class="block mb-1" for="products">Products</label>
                <select id="products" class="select2" required multiple data-live-search="true" name="products[]">
                </select>
            </div>
        </div>
        <div class="flex flex-wrap">
            <div class="w-[calc(100%+1rem)]">
                <button type="submit" class="inline-flex items-center h-8 px-4 m-2 text-sm text-indigo-100 duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800">Create</button>
            </div>
        </div>
    </form>
    <script>
        $(document).ready(function(){
            $('.select2').select2({
                width: '100%',
                minimumInputLength: 1,
                ajax: {
                    method: 'get',
                    url: '{{ route('products.jsonData') }}',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data, params) {
                        console.log(data.data);
                        return {
                            results: data.data
                        };
                    }
                }
            });
        });
    </script>
@endsection
