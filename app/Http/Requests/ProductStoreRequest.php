<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'in_storage' => 'boolean',
            'price' => 'required|numeric',
            'photo' => 'nullable|sometimes|file|image',
            'supplier_id' => 'required|exists:suppliers,id'
        ];
    }
}
