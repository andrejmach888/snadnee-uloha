<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Resources\ProductJsonResource;
use App\Models\Box;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('product.index', ['products' => Product::filters()->paginate($this->perPage)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create', ['suppliers' => Supplier::all()->pluck('name', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProductStoreRequest $request)
    {
        if(!Product::createProductWithFile($request->validated())){
            return back()->with('error', 'Something went wrong. Please try again.');
        }

        return redirect(route('products.index'))->with('success', 'Product was created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Product $product)
    {
        $boxes = Box::whereHas('products', function(Builder $query) use ($product){
            return $query->where('product_id', $product->id);
        })->paginate($this->perPage);
        return view('product.show', ['product' => $product, 'boxes' => $boxes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductStoreRequest $request
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ProductStoreRequest $request, Product $product)
    {
        if(!$product->updateProductWithFile($request->validated())){
            return back()->with('error', 'Something went wrong. Please try again.');
        }

        return back()->with('success', 'Product was updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product)
    {
        if(!$product->removeProductWithFile()){
            return back()->with('error', 'Something went wrong. Please try again.');
        }

        return redirect(route('products.index'))->with('success', 'Product was removed successfully.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function jsonData(Request $request)
    {
        if ($request->exists('search')) {
            $collection = Product::where('name', 'LIKE', '%' . $request->get('search') . '%')->get(['id', 'name']);
        } else {
            $collection = Product::all(['id', 'name']);
        }
        return ProductJsonResource::collection($collection);
    }
}
