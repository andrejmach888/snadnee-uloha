<?php

namespace App\Http\Controllers;

use App\Http\Requests\BoxStoreRequest;
use App\Models\Box;
use App\Models\Product;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('box.index', ['boxes' => Box::paginate($this->perPage)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('box.create', ['products' => Product::all()->pluck('name', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BoxStoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BoxStoreRequest $request)
    {
        if(!Box::createBoxWithPivot($request->validated())){
            return back()->with('error', 'Something went wrong. Please try again.');
        }

        return redirect(route('boxes.index'))->with('success', 'Box was created successfully.');
    }
}
