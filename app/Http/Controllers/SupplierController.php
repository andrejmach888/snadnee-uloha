<?php

namespace App\Http\Controllers;

use App\Http\Resources\SupplierJsonResource;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplier.index', ['suppliers' => Supplier::paginate($this->perPage)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function jsonData(Request $request)
    {
        if ($request->exists('search')) {
            $collection = Supplier::where('name', 'LIKE', '%' . $request->get('search') . '%')->get(['id', 'name']);
        } else {
            $collection = Supplier::all(['id', 'name']);
        }
        return SupplierJsonResource::collection($collection);
    }
}
