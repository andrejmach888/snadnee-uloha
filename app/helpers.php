<?php

if (!function_exists('explodeImplodeString')) {
    function explodeImplodeString($explodeDelimiter, $glue, $string){
        return implode($glue, explode($explodeDelimiter, $string));
    }
}
