<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'in_storage',
        'price',
        'photo',
        'supplier_id',
    ];

    protected $casts = [
        'in_storage' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boxes()
    {
        return $this->belongsToMany(Box::class)->using(ProductBox::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y H:i:s');
    }

    public function scopeFilters(Builder $query)
    {
        if (request()->has('search') && !empty(request()->get('search'))) {
            $query->where(function (Builder $q) {
                $q->where('id', request()->get('search'))
                    ->orWhere('name', 'like', '%' . request()->get('search') . '%')
                    ->orWhereHas('supplier', function (Builder $qe) {
                        $qe->where('name', 'like', '%' . request()->get('search') . '%');
                    });
            });
        }
        if (request()->has('in_storage') && request()->get('in_storage') == 1){
            $query->where('in_storage', true);
        }
        if (request()->has('order_by') && !empty(request()->get('order_by'))){
            $order = explode(',',request()->get('order_by'));
            $query->orderBy($order[0], $order[1] ?? 'asc');
        }
        return $query;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function createProductWithFile($data)
    {
        DB::beginTransaction();
        try {
            self::create([
                'name' => $data['name'],
                'in_storage' => $data['in_storage'] ?? 0,
                'price' => $data['price'],
                'photo' => !empty($data['photo']) ? Storage::disk('public')->putFile('products', $data['photo']) : null,
                'supplier_id' => $data['supplier_id']
            ]);

            DB::commit();
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            return false;
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateProductWithFile($data)
    {
        DB::beginTransaction();
        try {
            $updatingData = [
                'name' => $data['name'],
                'in_storage' => $data['in_storage'] ?? 0,
                'price' => $data['price'],
                'supplier_id' => $data['supplier_id']
            ];
            if (!empty($data['photo'])) {
                $updatingData['photo'] = Storage::disk('public')->putFile('products', $data['photo']);
            }
            $this->update($updatingData);

            DB::commit();
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            return false;
        }
    }

    public function removeProductWithFile()
    {
        DB::beginTransaction();
        try {
            $hasFile = false;
            if (!empty($this->photo)) {
                $hasFile = true;
            }
            $this->delete();
            if ($hasFile) {
                Storage::disk('public')->delete($this->photo);
            }
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            return false;
        }
    }
}
