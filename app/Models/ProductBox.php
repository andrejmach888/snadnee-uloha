<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Relations\Pivot;

class ProductBox extends Pivot
{
    public $incrementing = true;

    protected $table = 'product_boxes';

    protected $guarded = ['id'];
}
