<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Box extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'discount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_boxes')->using(ProductBox::class)->withTimestamps();
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y H:i:s');
    }

    /**
     * @param $data
     * @return bool
     */
    public static function createBoxWithPivot($data)
    {
        DB::beginTransaction();
        try {
            self::create([
                'name' => $data['name'],
                'discount' => $data['discount']
            ])->products()->attach($data['products']);

            DB::commit();
            return true;
        } catch (\Exception $exception){
            Log::error($exception);
            DB::rollBack();
            return false;
        }
    }
}
