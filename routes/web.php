<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ProductController;
use \App\Http\Controllers\BoxController;
use \App\Http\Controllers\SupplierController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::permanentRedirect('/', '/products');

Route::get('products/jsonData', [ProductController::class, 'jsonData'])->name('products.jsonData');
Route::resource('products', ProductController::class);
Route::get('boxes', [BoxController::class, 'index'])->name('boxes.index');
Route::post('boxes', [BoxController::class, 'store'])->name('boxes.store');
Route::get('boxes/create', [BoxController::class, 'create'])->name('boxes.create');
Route::get('suppliers/jsonData', [SupplierController::class, 'jsonData'])->name('suppliers.jsonData');
Route::get('suppliers', [SupplierController::class, 'index'])->name('suppliers.index');

