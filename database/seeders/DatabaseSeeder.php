<?php

namespace Database\Seeders;

use App\Models\Box;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Supplier::factory(100)->create();
        Product::factory(10000)->create();
        Box::factory(1000)->create();

        $products = Product::all();
        Box::all()->each(function ($box) use ($products){
            $box->products()->attach(
                $products->random(rand(1, 5))->pluck('id')->toArray()
            );
        });
    }
}
