<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->withFaker()->word(),
            'in_storage' => $this->withFaker()->boolean(80),
            'price' => $this->withFaker()->randomFloat(2, 0, 50),
            'photo' => null,
            'supplier_id' => Supplier::all()->random()->id,
        ];
    }
}
